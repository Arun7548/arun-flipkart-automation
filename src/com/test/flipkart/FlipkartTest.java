package com.test.flipkart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
public class FlipkartTest {
    static String driverPath = "/home/arun/Documents/selenium/BrowserDrivers/chromedriver_linux64 (1)/";
    public static void main(String[] args) throws InterruptedException {
        try {
            System.setProperty("webdriver.chrome.driver", driverPath+"chromedriver");
            WebDriver driver = new ChromeDriver();

            driver.get("https://www.flipkart.com/");
            driver.manage().window().maximize();
            driver.findElement(By.xpath("//button[@class='_2KpZ6l _2doB4z']")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//input[@class='_3704LK']")).sendKeys("grocery");
            Thread.sleep(1000);
            driver.findElement(By.xpath("//button[@class = 'L0Z3Pu']")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//a[@class ='s1Q9rs']")).click();

            ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
            driver.switchTo().window(tabs.get(tabs.size() - 1));

            driver.findElement(By.id("pincodeInputId")).sendKeys("560034");
            driver.findElement(By.className("_2P_LDn")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//a[@class = '_1fGeJ5 PP89tw']")).click();
            Thread.sleep(1000);

            Actions action = new Actions(driver);
            WebElement element = driver.findElement(By.xpath("//button[@class='_2KpZ6l _2U9uOA _3v1-ww _27Cjkl']"));
            action.moveToElement(element).click().perform();
            Thread.sleep(1000);
            element = driver.findElement(By.xpath("//button[@class='_2KpZ6l _2U9uOA _3v1-ww undefined _27Cjkl']"));
            action.moveToElement(element).click().perform();
            Thread.sleep(1000);
            driver.quit();
        }catch (Exception e){
            System.out.println(e);
        }



    }
}
